/*
simple selector

#id

.class

element

*/

class HTMLArray extends Array {
    constructor(array = []) {
        if (array instanceof HTMLCollection) {
            var aux = [];
            for (var elem of array) {
                aux.push(aux);
            }
            array = aux;
        }
        super(...array);
    }

    add() {
        for (var a of arguments) {
            if ((a instanceof Array) || (a instanceof HTMLCollection) || (a instanceof HTMLArray)) {
                for (var i of a) {
                    this.add(i);
                }
            } else if (a instanceof HTMLElement || (a instanceof Text)) {
                this.push(a);
            }
        }
    }

    remove() {
        for (var i = 0; i < this.length; i++) {
            this[i].remove();
        }
        return this;
    }

    select() {
        var result = new HTMLArray();
        for (var i = 0; i < this.length; i++) {
            result.add(simpleHTML.select(this[i], ...arguments));
        }
        return result;
    }

    append() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.append(this[i], ...arguments);
        }
        return this;
    }

    attr() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.attr(this[i], ...arguments);
        }
        return this;
    }

    css() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.css(this[i], ...arguments);
        }
        return this;
    }

    event() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.event(this[i], ...arguments);
        }
        return this;
    }

    classes() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.classes(this[i], ...arguments);
        }
        return this;
    }

    addClass() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.addClass(this[i], ...arguments);
        }
        return this;
    }

    removeClass() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.removeClass(this[i], ...arguments);
        }
        return this;
    }

    removeAllClasses() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.removeAllClasses(this[i], ...arguments);
        }
        return this;
    }

    clear() {
        for (var i = 0; i < this.length; i++) {
            simpleHTML.clear(this[i], ...arguments);
        }
        return this;
    }

    child() {
        var result = new HTMLArray();
        for (var i = 0; i < this.length; i++) {
            result.add(simpleHTML.child(this[i], ...arguments));
        }
        return result;
    }

    parent() {
        var result = new HTMLArray();
        for (var i = 0; i < this.length; i++) {
            result.add(simpleHTML.parent(this[i], ...arguments));
        }
        return result;
    }
}

//The selector strings will be aplied to all elements previosly added to the result list, if none are found then the documentelement will be used

function simpleHTML() {
    var result = new HTMLArray();
    if (arguments.length) {
        for (var a of arguments) {
            if ((a instanceof Array) || (a instanceof HTMLCollection) || (a instanceof HTMLArray)) {
                for (var i of a) {
                    result.push(i);
                }
            } else if (a instanceof HTMLElement) {
                result.push(a);
            } else if (typeof a == 'string') {
                if (!result.length) {
                    result.add(document.documentElement);
                }
                result = simpleHTML.select(result, a);
                if (result.length == 1) {
                    return result[0];
                }
                return result;
            }
        }
    }

    return result;
}

simpleHTML._hasClasses = function (elem, classes) {
    var hasClasses = true;
    for (var c of classes) {
        if (!elem.classList.contains(c)) {
            hasClasses = false;
            break;
        }
    }
    return hasClasses;
}

simpleHTML._paserSelector = function (selector) {
    var parsedSelector = {};
    selector = selector.trim();
    if (selector.startsWith('#')) {
        parsedSelector.type = 'id';
        parsedSelector.id = selector.substring(1);
    } else if (selector.startsWith('.')) {
        parsedSelector.type = 'class';
        parsedSelector.classes = selector.substring(1).split('.');
    } else {
        parsedSelector.type = 'tag';
        parsedSelector.classes = selector.split('.');
        parsedSelector.tag = parsedSelector.classes.shift().toUpperCase();
    }
    return parsedSelector;
}

simpleHTML._tagList = [];

simpleHTML.parseSelector = function (selector) {
    var parsedSelector = {};
    var selectorList = selector.split(',');
    if (selectorList.length > 1) {
        parsedSelector.multiple = true;
        parsedSelector.list = [];
        for (var s of selectorList) {
            parsedSelector.list.push(simpleHTML._paserSelector(s));
        }
    } else {
        parsedSelector = simpleHTML._paserSelector(selector);
        parsedSelector.multiple = false;
    }
    return parsedSelector
}

simpleHTML.select = function (elem, selector) {
    if (typeof selector == 'string') {
        selector = simpleHTML.parseSelector(selector);
    }
    if (elem instanceof HTMLArray) {
        return elem.select(selector);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        return elem.select(selector);
    } else {
        var result = new HTMLArray();
        if (selector.multiple) {
            for (var i = 0; i < selector.list.length; i++) {
                var s = selector.list[i];
                if (s.type == 'id') {
                    if (elem.id == s.id) {
                        result.add(elem);
                        selector.list.splice(i, 1); //Removes the id selector from the list
                        i--;
                    }
                } else {
                    switch (s.type) {
                        case 'class':
                            if (simpleHTML._hasClasses(elem, s.classes)) {
                                result.add(elem);
                            }
                            break;
                        case 'tag':
                            if (elem.tagName == s.tag && simpleHTML._hasClasses(elem, s.classes)) {
                                result.add(elem);
                            }
                            break;
                    }
                }
            }
            if (selector.list.length) {
                for (var child of elem.children) {
                    result.add(simpleHTML.select(child, selector));
                }
            }
        } else {
            if (selector.type == 'id') {
                if (elem.id == selector.id) {
                    result.add(elem);
                } else {
                    for (var child of elem.children) {
                        var select = simpleHTML.select(child, selector);
                        if (select.length) {
                            result.add(select);
                            break;
                        }
                    }
                }
            } else {
                switch (selector.type) {
                    case 'class':
                        if (simpleHTML._hasClasses(elem, selector.classes)) {
                            result.add(elem);
                        }
                        break;
                    case 'tag':
                        if (elem.tagName == selector.tag && simpleHTML._hasClasses(elem, selector.classes)) {
                            result.add(elem);
                        }
                        break;
                }
                for (var child of elem.children) {
                    result.add(simpleHTML.select(child, selector));
                }
            }
        }
        return result;
    }
}

simpleHTML.append = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.append(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.append(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if ((arg instanceof Array) || (arg instanceof HTMLCollection)) {
                    simpleHTML.append(elem, ...arg);
                } else if (arg instanceof Function) {
                    switch (arg) {
                        case simpleHTML.attr:
                            var aux = [];
                            i++;
                            while (i < arguments.length && !(arguments[i] instanceof Function)) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.attr(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.css:
                            var aux = [];
                            i++;
                            while (i < arguments.length && !(arguments[i] instanceof Function)) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.css(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.event:
                            var aux = [];
                            i++;
                            while (i < arguments.length && arguments[i] != simpleHTML.attr && arguments[i] != simpleHTML.css && arguments[i] != simpleHTML.event && arguments[i] != simpleHTML.classes) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.event(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.classes:
                            var aux = [];
                            i++;
                            while (i < arguments.length && !(arguments[i] instanceof Function)) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.classes(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.addClass:
                            var aux = [];
                            i++;
                            while (i < arguments.length && !(arguments[i] instanceof Function)) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.addClass(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.removeClass:
                            var aux = [];
                            i++;
                            while (i < arguments.length && !(arguments[i] instanceof Function)) {
                                aux.push(arguments[i]);
                                i++;
                            }
                            simpleHTML.removeClass(elem, ...aux);
                            if (i < arguments.length) {
                                i--;
                            }
                            break;
                        case simpleHTML.removeAllClasses:
                            simpleHTML.removeAllClasses(elem);
                        case simpleHTML.clear:
                            simpleHTML.clear(elem);
                        case simpleHTML.element:
                            break;
                        default:
                            elem.appendChild(arg());
                    }
                } else if ((arg instanceof HTMLElement) || (arg instanceof Text)) {
                    elem.appendChild(arg);
                } else {
                    simpleHTML.append(elem, text(arg));
                }
            }
        }
    }
    return elem;
}

simpleHTML.attr = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.attr(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.attr(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if (typeof arg == 'object') {
                    for (var a in arg) {
                        elem.setAttribute(a, arg[a]);
                    }
                } else {
                    i++
                    elem.setAttribute(arg, arguments[i]);
                }
            }
        }
    }
    return elem;
}

simpleHTML.css = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.css(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.css(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if (typeof arg == 'object') {
                    for (var a in arg) {
                        elem.style[a] = arg[a];
                    }
                } else {
                    i++
                    elem.style[arg] = arguments[i];
                }
            }
        }
    }
    return elem;
}

simpleHTML.event = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.event(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.event(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if (typeof arg == 'object') {
                    for (var a in arg) {
                        elem.addEventListener(a, arg[a]);
                    }
                } else {
                    i++
                    elem.addEventListener(arg, arguments[i]);
                }
            }
        }
    }
    return elem;
}

simpleHTML.addClass = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.addClass(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.addClass(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if (arg instanceof Array) {
                    simpleHTML.addClass(elem, ...arg);
                } else {
                    elem.classList.add(arg);
                }
            }
        }
    }
    return elem;
}

//Note: if no elements are given then all instances of the classes will be removed
//TODO ^ this
simpleHTML.removeClass = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.removeClass(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.removeClass(...args);
    } else {
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            if (arg) {
                if (arg instanceof Array) {
                    simpleHTML.removeClass(elem, ...arg);
                } else {
                    elem.classList.remove(arg);
                }
            }
        }
    }
    return elem;
}

simpleHTML.removeAllClasses = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.removeAllClasses(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.removeAllClasses(...args);
    } else {
        elem.classList.value = '';
    }
    return elem;
}

simpleHTML.classes = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.classes(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.classes(...args);
    } else {
        simpleHTML.removeAllClasses(elem);
        simpleHTML.addClass(...arguments);
    }
    return elem;
}

simpleHTML.clear = function (elem) {
    if (elem instanceof HTMLArray) {
        var args = [...arguments];
        args.shift();
        elem.clear(...args);
    } else if ((elem instanceof Array) || (elem instanceof HTMLCollection)) {
        elem = new HTMLArray(elem);
        var args = [...arguments];
        args.shift();
        elem.clear(...args);
    } else {
        while (elem.firstChild) {
            elem.removeChild(elem.firstChild);
        }
    }
    return elem;
}


simpleHTML.child = function (parent) {
    if (parent) {
        var child = parent;
        for (var i = 1; i < arguments.length; i++) {
            var arg = arguments[i];
            child = child.children[arg];
            if (!child) {
                return null;
            }
        }
        return child
    }
    return null;
}

simpleHTML.parent = function (child, degree = 0) {
    if (child) {
        if (child.parentElement) {
            if (degree > 0) {
                return child.parentElement.parent(degree - 1);
            }
            return child.parentElement;
        }
    }
    return null;
}

simpleHTML.addNewTag = function (tag) {
    if (arguments.length == 1) {
        simpleHTML[tag] = function () {
            return simpleHTML.append(document.createElement(tag), ...arguments);
        }
    } else if (arguments.length % 2 == 1) {
        var attributes = [];
        for (var i = 1; i < arguments.length; i += 2) {
            attributes[(i - 1) / 2] = {
                name: arguments[i],
                value: arguments[i + 1]
            };
        };
        simpleHTML[tag] = function () {
            var elem = document.createElement(tag);
            var args = [...arguments];
            for (var i = 0; i < attributes.length; i++) {
                elem.setAttribute(attributes[i].name, (arguments[i] == undefined ? attributes[i].value : arguments[i]));
                args.shift();
            }
            return simpleHTML.append(elem, ...args);
        }
    } else {
        return //Invalid number of arguments
    }
    simpleHTML._tagList.push(tag);
}

simpleHTML.element = function (elem) {
    var args = [...arguments];
    args.shift();
    return simpleHTML.append(document.createElement(elem), ...args);
}

simpleHTML.text = function (text) {
    text = text.toString();
    var list = text.split('\n');
    if (list.length > 1) {
        var result = new HTMLArray();
        result.add(document.createTextNode(list[0]));
        for (var i = 1; i < list.length; i++) {
            result.add(simpleHTML.br());
            result.add(document.createTextNode(list[i]));
        }
        return result;
    } else {
        return document.createTextNode(text);
    }
}

simpleHTML.addCreatorsToGlobal = function () {
    window.element = simpleHTML.element;
    for (var tag of this._tagList) {
        window[tag] = this[tag];
    }
    window.text = simpleHTML.text;
}

simpleHTML.addFunctionsToGlobal = function () {
    window.select = simpleHTML.select;
    window.append = simpleHTML.append;
    window.attr = simpleHTML.attr;
    window.css = simpleHTML.css;
    window.event = simpleHTML.event;
    window.classes = simpleHTML.classes;
    window.addClass = simpleHTML.addClass;
    window.removeClass = simpleHTML.removeClass;
    window.removeAllClasses = simpleHTML.removeAllClasses;
    window.clear = simpleHTML.clear;
    window.child = simpleHTML.child;
    window.parent = simpleHTML.parent;
}

simpleHTML.addFunctionsToElements = function () {
    HTMLElement.prototype.select = function () {
        return simpleHTML.select(this, ...arguments);
    };
    HTMLElement.prototype.append = function () {
        return simpleHTML.append(this, ...arguments);
    };
    HTMLElement.prototype.attr = function () {
        return simpleHTML.attr(this, ...arguments);
    };
    HTMLElement.prototype.css = function () {
        return simpleHTML.css(this, ...arguments);
    };
    HTMLElement.prototype.event = function () {
        return simpleHTML.event(this, ...arguments);
    };
    HTMLElement.prototype.classes = function () {
        return simpleHTML.classes(this, ...arguments);
    };
    HTMLElement.prototype.addClass = function () {
        return simpleHTML.addClass(this, ...arguments);
    };
    HTMLElement.prototype.removeClass = function () {
        return simpleHTML.removeClass(this, ...arguments);
    };
    HTMLElement.prototype.removeAllClasses = function () {
        return simpleHTML.removeAllClasses(this, ...arguments);
    };
    HTMLElement.prototype.clear = function () {
        return simpleHTML.clear(this, ...arguments);
    };
    HTMLElement.prototype.child = function () {
        return simpleHTML.child(this, ...arguments);
    };
    HTMLElement.prototype.parent = function () {
        return simpleHTML.parent(this, ...arguments);
    };
}

simpleHTML.addNewTag('div');
simpleHTML.addNewTag('table');
simpleHTML.addNewTag('tr');
simpleHTML.addNewTag('th');
simpleHTML.addNewTag('td');
simpleHTML.addNewTag('span');
simpleHTML.addNewTag('canvas');
simpleHTML.addNewTag('ul');
simpleHTML.addNewTag('li');
simpleHTML.addNewTag('p');
simpleHTML.addNewTag('br');
simpleHTML.addNewTag('a', 'href', '#');
simpleHTML.addNewTag('input', 'type', 'text');
simpleHTML.addNewTag('img', 'src', '');